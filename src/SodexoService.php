<?php
/**
 * Created by PhpStorm.
 * User: olliv
 * Date: 12.1.2018
 * Time: 11.12
 */

class SodexoService
{
    private $_place_id ="";
    private $_lang = '';
    private $_api_uri = "https://www.sodexo.fi/ruokalistat/output/daily_json/";
    //                                                                       ID/VVVV/KK/PP/lang

    function __construct($lang = "fi", $place_id = "31332"){
        $this->_lang = $lang;
        $this->_place_id = $place_id;
    }

    function getDayMenu($year, $month, $day){
        $raw_content = file_get_contents($this->_api_uri . $this->_place_id . "/" . $year . "/" . $month . "/" . $day . "/" . $this->_lang);
        $json = json_decode($raw_content, true);

        // add date to meta-information
        $json['meta']['date'] = $day . "." . $month . "." . $year;

        return $json;
    }

    function getWeekMenu($year, $week){
        $start = new DateTime();
        $start->setISODate($year, $week);
        $foods = array();
        for($i = 0; $i < 7; $i++){
            $daymenu = $this->getDayMenu($year, $start->format("n"), $start->format("j"));
            if(count($daymenu['courses']) > 0){
                $foods[$i] = $daymenu;
            }
            $start->modify("+1 day");
        }

        return $foods;
    }
}