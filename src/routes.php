<?php

include("SodexoService.php");
use Slim\Http\Request;
use Slim\Http\Response;

// Routes

$app->get('/', function (Request $request, Response $response, array $args) {
    $ss = new SodexoService("fi");
    $args['menu'] = $ss->getDayMenu(date("Y"), date("n"), date("j"));
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->get('/week', function(Request $request, Response $response, array $args){

    $ss = new SodexoService("fi");
    $foods = $ss->getWeekMenu(date("Y"), date("W"));
    $args['foods'] = $foods;

    $dt = new DateTime();
    $dt->setISODate(date("Y"), date("W"));
    $dt->modify("+1 week");
    $nextweek = $dt->format("W");
    $lastweek = null;

    $args['year'] = $dt->format("Y");
    $args['nextweek'] = ltrim($nextweek, 0);
    $args['lastweek'] = $lastweek;

    return $this->renderer->render($response, 'week.phtml', $args);
});

$app->get('/week/{year}/{week}', function(Request $request, Response $response, array $args){
    $year = $args['year'];
    $week = $args['week'];


    $dt = new DateTime();
    $dt->setISODate($year, $week);
    $dt->modify("+1 week");
    $nextweek = $dt->format("W");
    $lastweek = null;
    $dt->modify("-2 week");
    if(ltrim($dt->format("W"), 0) <= ltrim(date("W"), 0)){
        $lastweek = $dt->format("W");
    }

    $args['year'] = $year;
    $args['nextweek'] = ltrim($nextweek, 0);
    $args['lastweek'] = ltrim($lastweek, 0);

    $ss = new SodexoService("fi");
    $foods = $ss->getWeekMenu($year, $week);
    $args['foods'] = $foods;
    return $this->renderer->render($response, 'week.phtml', $args);
});
