# Sodexo-menu

Sodexo-menu hakee ruokalistoja Sodexo Oy:n JSON-apista, ja esittää ne hieman mobiiliystävällisemmin kuin Sodexon omat sivut.

## Install the Application

Run this command from the directory in which you want to install your new Slim Framework application.

    php composer.phar create-project slim/slim-skeleton sodexo-menu


* Point your virtual host document root to your new application's `public/` directory.
* Ensure `logs/` is web writeable.

To run the application in development, you can also run this command. 

	php composer.phar start

Run this command to run the test suite

	php composer.phar test

